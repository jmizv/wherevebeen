# wherevebeen

**Where have I been so far?**

Ever asked this question? And you know these maps where you can rub parts of it to mark
where you actually have been? Ever wanted this be digital?

Track where you have been so far and generated a map from it viewable in http://umap.openstreetmap.fr.

## Introduction

Types of visits, e.g. places where you live and go by regularly
because of school or work or vacations or other kind of trips. Maybe former cities where you have
lived.

## Covering

* **points** to mark a spot where you have been, maybe some touristic attraction
* **lines** to mark a street or way where you have walked, hiked or travelled
* **areas** to mark a region where you have been.

## Use cases

